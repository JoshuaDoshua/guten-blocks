/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

//import '@fortawesome/fontawesome-pro/js/all.min.js'

import { library } from '@fortawesome/fontawesome-svg-core'
import {
	faSquareFull,
	faPlus,
	faMinus,
	faArrowRight
} from '@fortawesome/pro-light-svg-icons'
library.add(
	faSquareFull,
	faPlus,
	faMinus,
	faArrowRight
)

import './style.scss'
import './util/Decorations/style.scss'

import './registry'

import './button/block'
import './heading/block'
import './split/block'
import './curtain-slider/block'
import './box-links/block'
import './accordion/block'
import './up-next/block'
import './outline-box/block'
import './number-fact/block'
import './by-the-numbers/block'
import './hero/block'
import './children/block'
import './q-a/block'
import './image-grid-cta/block'
import './columns/block'
import './columns/components/column'
import './overture/block'
import './info-finder/block'
import './icon-list/block'
