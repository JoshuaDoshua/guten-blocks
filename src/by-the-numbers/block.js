import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

import attributes from './attributes'
import ByTheNumbers from './components/ByTheNumbers'

registerBlockType('power-blocks/by-the-numbers', {
	title: 'By the Numbers',
	icon: 'screenoptions',
	category: 'power-blocks',
	keywords: ['power', 'number'],
	attributes,
	edit(props) {
		return <ByTheNumbers {...props} />
	},
	save(props) {
		return <ByTheNumbers.Content {...props} />
	}
})
