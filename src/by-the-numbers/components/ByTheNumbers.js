const { Component, Fragment } = wp.element
const { InspectorControls, InnerBlocks } = wp.editor

import {ImageSelector} from '../../util/ImageSelector'

export default class ByTheNumbers extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const { attributes } = this.props
		return (
			<Fragment>
				<InspectorControls>
					<br />
					<ImageSelector {...this.props} />
				</InspectorControls>
				<div className="bynum__wrap">
					<section className="bynum"
						style={{backgroundImage:'url(' + attributes.imageUrl + ')'}}
						>
						<InnerBlocks
							template={[
								['power-blocks/number-fact', {}],
								['power-blocks/number-fact', {}],
								['power-blocks/number-fact', {}],
								['power-blocks/number-fact', {}],
								['power-blocks/button', {}]
							]}
							templateLock="all"
							/>
					</section>
				</div>
			</Fragment>
		)
	}

	static Content(props) {
		const { attributes } = props

		return (
			<div className="bynum__wrap">
				<section className="bynum"
					style={{backgroundImage:'url(' + attributes.imageUrl + ')'}}
					>
					<InnerBlocks.Content />
				</section>
			</div>
		)
	}
}
