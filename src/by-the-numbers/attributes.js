import { imageAttributes } from '../util/ImageSelector'

export default {
	...imageAttributes
}
