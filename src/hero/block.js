import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

import attributes from './attributes'
import Hero from './components/Hero'

registerBlockType('power-blocks/hero', {
	title: 'Hero',
	icon: 'format-image',
	category: 'power-blocks',
	keywords: ['power', 'hero'],
	attributes,
	edit(props) {
		return <Hero {...props} />
	},
	save(props) {
		return <Hero.Content {...props} />
	}
})
