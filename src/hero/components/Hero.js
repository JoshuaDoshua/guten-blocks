const { Component, Fragment } = wp.element
const { RichText, InspectorControls, InnerBlocks } = wp.editor

import ImageSelector from '../../util/ImageSelector'

export default class Hero extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const {
			attributes,
			setAttributes
		} = this.props

		return (
			<Fragment>
				<InspectorControls>
					<br/>
					<ImageSelector {...this.props} />
				</InspectorControls>
				<div className="hero__wrap"
					style={{backgroundImage:'url(' + attributes.imageUrl + ')'}}
					>
					<section
						className="hero"
						>
						<div className="hero__body">
							<h2 className="hero__headline">
								<RichText
									className="hero__headline--span"
									tagName="span"
									value={attributes.headline}
									onChange={value => setAttributes({headline: value})}
									placeholder="Headline"
									keepPlaceholderOnFocus
									/>
							</h2>
							<RichText
								className="hero__copy"
								tagName="p"
								value={attributes.copy}
								onChange={value => setAttributes({copy: value})}
								formattingControls={[]}
								placeholder="Copy"
								keepPlaceholderOnFocus
								/>
							<InnerBlocks
								template={[['power-blocks/button', {}]]}
								templateLock
								/>
						</div>
					</section>
				</div>
			</Fragment>
		)
	}

	static Content(props) {
		const { attributes } = props

		return (
			<div className="hero__wrap">
				<section className="hero">
					<div className="hero__body">
						<h2 className="hero__headline">
							<span className="hero__headline--span">{attributes.headline}</span>
						</h2>
						<p className="hero__copy">{attributes.copy}</p>
						<InnerBlocks.Content />
					</div>
				</section>
			</div>
		)
	}
}
