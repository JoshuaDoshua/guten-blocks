import { imageAttributes } from '../util/ImageSelector'

export default {
	headline: {
		type: 'html',
		selector: '.hero__headline--span'
	},
	copy: {
		type: 'html',
		selector: '.hero__copy'
	},
	...imageAttributes
}
