export default {
	subhead: {
		source: 'text',
		selector: '.heading__subhead'
	},
	headline: {
		source: 'text',
		selector: '.heading__headline'
	},
	alignment: {
		type: 'text',
		default: 'center'
	}
}
