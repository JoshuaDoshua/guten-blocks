const { Component } = wp.element
const { SVG, Path } = wp.components

export default class HeadingIcon extends Component {
	render() {
		return (
			<SVG xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">
				<Path d="M5 4v3h5.5v12h3V7H19V4z" />
			</SVG>
		)
	}
}
