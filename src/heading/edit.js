const { Component, Fragment } = wp.element
const {
	AlignmentToolbar,
	BlockControls,
	RichText
} = wp.editor

import {
	getWrapperClasses,
	getContainerClasses,
	getElementClasses
} from './classes'

export default class HeadingEdit extends Component {
	constructor() {
		super(...arguments)
	}

	render() {
		const {
			attributes,
			setAttributes
		} = this.props

		return (
			<Fragment>
				<BlockControls>
					<AlignmentToolbar
						value={attributes.alignment}
						onChange={value => setAttributes({alignment: value})}
						/>
				</BlockControls>
				<div className={getWrapperClasses(attributes)}>
					<div className={getContainerClasses(attributes)}>
						<RichText
							formattingControls={[]}
							tagName="strong"
							className={getElementClasses('subhead')}
							onChange={newSubhead => {setAttributes({subhead: newSubhead})}}
							placeholder={'Subhead'}
							keepPlaceholderOnFocus={true}
							value={attributes.subhead}
							/>
						<RichText
							formattingControls={[]}
							tagName="h2"
							className={getElementClasses('headline')}
							onChange={newHeadline => {setAttributes({headline: newHeadline})}}
							placeholder={'Headline'}
							keepPlaceholderOnFocus={true}
							value={attributes.headline}
							/>
					</div>
				</div>
			</Fragment>
		)
	}
}
