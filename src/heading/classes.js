import classnames from 'classnames'
import bem from '../util/bem'

const block = 'heading'

const getWrapperClasses = attributes => bem(block, 'wrap')
const getContainerClasses = attributes => bem(block, null, [attributes.alignment])
const getElementClasses = el => classnames(
	el,
	bem(block, el)
)

export {
	getWrapperClasses,
	getContainerClasses,
	getElementClasses
}
