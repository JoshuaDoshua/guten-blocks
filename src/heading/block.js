import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

import icon from './icon'
import attributes from './attributes'
import edit from './edit'

import {
	getWrapperClasses,
	getContainerClasses,
	getElementClasses
} from './classes'

registerBlockType('power-blocks/heading', {
	title: 'Heading',
	icon,
	category: 'power-blocks',
	keywords: ['power', 'heading'],
	attributes,
	edit,
	save({attributes}) {
		return (
			<div className={getWrapperClasses(attributes)}>
				<div className={getContainerClasses(attributes)}>
					<strong className={getElementClasses('subhead')}>
						{attributes.subhead}
					</strong>
					<h2 className={getElementClasses('headline')}>
						{attributes.headline}
					</h2>
				</div>
			</div>
		)
	}
})
