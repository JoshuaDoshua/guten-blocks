import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

import ChildrenBlock from './components/ChildrenBlock'

registerBlockType('power-blocks/children', {
	title: 'Children',
	icon: 'grid-view',
	category: 'power-blocks',
	keywords: ['power', 'children', 'grid'],
	attributes: {
		parent: {
			type: 'number'
		}
	},
	edit(props) {
		return <ChildrenBlock {...props} />
	},
	save() {
		return null
	}
})
