const { Component } = wp.element
const { ServerSideRender } = wp.components

const { getCurrentPostId } = wp.data.select('core/editor')
//const currentPostId = wp.data.select('core/editor').getCurrentPostId()

export default class ChildrenBlock extends Component {
	constructor() {
		super(...arguments)

		const currentPostId = getCurrentPostId()
		this.props.setAttributes({parent: currentPostId})
	}

	render() {
		const { attributes } = this.props

		if (!attributes.parent)
			return (<p>Loading...</p>)

		return (
			<ServerSideRender
				block="power-blocks/children"
				attributes={attributes}
				/>
		)
	}
}
