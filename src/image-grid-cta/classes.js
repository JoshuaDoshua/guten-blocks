import classnames from 'classnames'
import bem from '../util/bem'

const block = 'imagecta'

const getWrapperClasses = additionalClasses => classnames(
	additionalClasses,
	bem(block, 'wrap')
)

const getContainerClasses = () => block

const getItemClasses = isLarge => {
	const mod = isLarge ? 'lrg' : 'sml'
	return bem(block, 'item', [mod])
}

const getElementClasses = (el, mods = []) => bem(block, el, mods)

export {
	getWrapperClasses,
	getContainerClasses,
	getItemClasses,
	getElementClasses
}
