const { Component, Fragment } = wp.element
const {	InspectorControls, MediaUpload } = wp.editor
const { BaseControl, PanelBody, SelectControl } = wp.components

import getImageButton from '../../util/getImageButton'
import UrlForm from '../../util/UrlForm'

const gradientOptions = [{
	label: 'None', value: ''
}, {
	label: 'Green', value: 'green'
}, {
	label: 'Orange', value: 'orange'
}, {
	label: 'Teal', value: 'teal'
}, {
	label: 'Gray', value: 'gray'
}]

export default class ImageGridInspector extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const { items } = this.props.attributes

		return (
			<InspectorControls>
				<PanelBody
					title="Images"
					initialOpen={true}
					>
					<BaseControl label="Top Left">
						<MediaUpload
							onSelect={media => this.props.onChange(0, 'imageUrl', media.url)}
							type="image"
							value={items[0].imageUrl}
							render={({open}) => getImageButton(open, items[0].imageUrl)}
							/>
					</BaseControl>
					<SelectControl
						label="Gradient"
						value={items[0].gradient}
						options={gradientOptions}
						onChange={value => this.props.onChange(0, 'gradient', value)}
						/>
					<BaseControl label="Bottom Left">
						<MediaUpload
							onSelect={media => this.props.onChange(1, 'imageUrl', media.url)}
							type="image"
							value={items[1].imageUrl}
							render={({open}) => getImageButton(open, items[1].imageUrl)}
							/>
					</BaseControl>
					<SelectControl
						label="Gradient"
						value={items[1].gradient}
						options={gradientOptions}
						onChange={value => this.props.onChange(1, 'gradient', value)}
						/>
					<BaseControl label="Right">
						<MediaUpload
							onSelect={media => this.props.onChange(2, 'imageUrl', media.url)}
							type="image"
							value={items[2].imageUrl}
							render={({open}) => getImageButton(open, items[2].imageUrl)}
							/>
					</BaseControl>
					<SelectControl
						label="Gradient"
						value={items[2].gradient}
						options={gradientOptions}
						onChange={value => this.props.onChange(2, 'gradient', value)}
						/>
				</PanelBody>
			</InspectorControls>
		)
	}
}
