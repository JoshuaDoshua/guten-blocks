const { Component, Fragment } = wp.element
const { RichText } = wp.editor

import ImageGridInspector from './ImageGridInspector'
import UrlForm from '../../util/UrlForm'

import {
	getWrapperClasses,
	getContainerClasses,
	getItemClasses,
	getElementClasses
} from '../classes'

export default class ImageGridCta extends Component {
	constructor() {
		super(...arguments)

		this.changeItem = this.changeItem.bind(this)
	}

	changeItem(index, key, value) {
		const items = this.props.attributes.items.slice()

		if (index >= items.length) return
		if (value === "") value = null

		items[index] = Object.assign(items[index], {[key]: value})
		this.props.setAttributes({items})
	}

	getItemsContent() {
		const { attributes, isSelected, setAttributes } = this.props
		const { items } = attributes

		return (
			items.map((item, index) => {
				return (
					<a className={getItemClasses(item.isLarge)}
						href={item.url}
						style={{backgroundImage: 'url(' + item.imageUrl + ')'}}
						key={item.id + '-link'}
						onClick={e => e.preventDefault()}
						>
						<div className={getElementClasses('inner')}>
							<RichText
								index={index}
								key={item.id + '-subhead'}
								tagName="h4"
								className={getElementClasses('subhead')}
								value={item.subhead}
								onChange={value => this.changeItem(index, 'subhead', value)}
								placeholder="Subhead"
								keepPlaceholderOnFocus
								formattingControls={[]}
								/>
							<RichText
								index={index}
								key={item.id + '-headline'}
								tagName="h3"
								className={getElementClasses('headline')}
								value={item.headline}
								onChange={value => this.changeItem(index, 'headline', value)}
								placeholder="headline"
								keepPlaceholderOnFocus
								formattingControls={[]}
								/>
						</div>
						{isSelected && (
							<UrlForm
								{...this.props}
								urlValue={item.url}
								onChangeUrl={value => this.changeItem(index, 'url', value)}
							/>
						)}
					</a>
				)
			})
		)
	}

	render() {
		const {
			attributes,
			className,
			setAttributes
		} = this.props

		return (
			<Fragment>
				<ImageGridInspector
					{...this.props}
					onChange={this.changeItem}
					/>
				<div className={getWrapperClasses(className)}>
					<div className={getContainerClasses()}>
						{ this.getItemsContent() }
					</div>
				</div>
			</Fragment>
		)
	}

	static ItemsContent(attributes) {
		return (
			attributes.items.map((item,index) => {
				return (
					<a className={getItemClasses(item.isLarge)}
						href={item.url}
						style={{backgroundImage: 'url(' + item.imageUrl + ')'}}
						key={item.id + '-link'}
						>
						<div className={getElementClasses('inner')}>
							<h4 className={getElementClasses('subhead')}
								index={index}
								key={item.id + '-subhead'}
								>{item.subhead}</h4>
							<h3 className={getElementClasses('headline')}
								index={index}
								key={item.id + '-headline'}
								>{item.headline}</h3>
						</div>
					</a>
				)
			})
		)
	}

	static Content(props) {
		const { attributes, className } = props

		return (
			<div className={getWrapperClasses(className)}>
				<div className={getContainerClasses()}>
					{ ImageGridCta.ItemsContent(attributes) }
				</div>
			</div>
		)
	}
}
