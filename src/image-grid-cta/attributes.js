export default {
	items: {
		type: 'array',
		default: [{
			id: 'item-1',
			subhead: null,
			headline: null,
			url: null,
			imageUrl: null,
			gradient: null,
			isLarge: false
		}, {
			id: 'item-2',
			subhead: null,
			headline: null,
			url: null,
			imageUrl: null,
			gradient: null,
			isLarge: false
		}, {
			id: 'item-3',
			subhead: null,
			headline: null,
			url: null,
			imageUrl: null,
			gradient: null,
			isLarge: true
		}]
	},
}
