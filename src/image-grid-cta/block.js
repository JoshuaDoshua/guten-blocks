import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

import attributes from './attributes'
import ImageGridCta from './components/ImageGridCta'

registerBlockType('power-blocks/image-grid-cta', {
	title: 'Image Grid CTA',
	icon: 'images-alt2',
	category: 'power-blocks',
	keywords: ['power', 'cta', 'image'],
	attributes,
	edit(props) {
		return <ImageGridCta {...props} />
	},
	save(props) {
		return <ImageGridCta.Content {...props} />
	}
})
