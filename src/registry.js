// TODO: unregisterBlockStyle
// NOTE: there is no need to register you blocks
// simply including the script will include them in the inspector
// you may need to reference them here depending on
// how you manage the block registry

/**
 * CORE BLOCK REFEERENCE
 *
	// MUST INCLUDE
	"core/missing",
	"core/paragraph",
	// COMMON
	"core/image",
	"core/heading",
	"core/gallery",
	"core/list",
	"core/quote",
	"core/audio",
	"core/column",
	"core/cover",
	"core/file",
	"core/subhead",
	"core/video",
	// FORMATTING
	"core/code",
	"core/freeform", // classic editor
	"core/html",
	"core/preformatted",
	"core/pullquote",
	"core/table",
	"core/verse"
	// LAYOUT
	"core/button",
	"core/columns",
	"core/media-text",
	"core/more",
	"core/nextpage",
	"core/separator",
	"core/spacer",
	"core/text-columns"
	// WIDGETS
	"core/shortcode",
	"core/archives",
	"core/categories",
	"core/latest-comments",
	"core/latest-posts"
	// EMBED
	"core/embed",
	"core-embed/twitter",
	"core-embed/youtube",
	"core-embed/facebook",
	"core-embed/instagram",
	"core-embed/wordpress",
	"core-embed/soundcloud",
	"core-embed/spotify",
	"core-embed/flickr",
	"core-embed/vimeo",
	"core-embed/animoto",
	"core-embed/cloudup",
	"core-embed/collegehumor",
	"core-embed/dailymotion",
	"core-embed/funnyordie",
	"core-embed/hulu",
	"core-embed/imgur",
	"core-embed/issuu",
	"core-embed/kickstarter",
	"core-embed/meetup-com",
	"core-embed/mixcloud",
	"core-embed/photobucket",
	"core-embed/polldaddy",
	"core-embed/reddit",
	"core-embed/reverbnation",
	"core-embed/screencast",
	"core-embed/scribd",
	"core-embed/slideshare",
	"core-embed/smugmug",
	"core-embed/speaker",
	"core-embed/speaker-deck",
	"core-embed/ted",
	"core-embed/tumblr",
	"core-embed/videopress",
	"core-embed/wordpress-tv",
*/

function getRegisteredCoreBlocks() {
	return wp.blocks.getBlockTypes().filter(type => {
		return (type.name.indexOf('core') > -1)
	})
}

// we only remove core blocks here
// to allow for OUR blocks and other plugins
// can use in conjuction with blacklistBlocks
function whitelistBlocks(registered, allowed) {
	registered.filter(block => {
		return !allowed.includes(block.name)
	}).forEach(block => {
		wp.blocks.unregisterBlockType(block.name)
	})
}

function blacklistBlocks(disallowed) {
	disallowed.forEach(block => {
		wp.blocks.unregisterBlockType(block)
	})
}

const whitelist = [
	'core/missing', // MUST INCLUDE
	'core/paragraph', // MUST INCLUDE
	'core/image',
	'core/table',
	'core/gallery',
	'core/list',
	'core/file',
	'core/video',
	'core/freeform', // classic editor
	'core/separator',
	'core/spacer',
	'core/shortcode',
	'core/embed',
	'core-embed/youtube',
	'core-embed/vimeo',
]
const blacklist = [
	'core/button'
]

wp.domReady(() => {
	setTimeout(function() {
		const coreBlockTypes = getRegisteredCoreBlocks()
		whitelistBlocks(coreBlockTypes, whitelist)
		// or
		// blacklist(blacklist)
	}, 1000)
})
