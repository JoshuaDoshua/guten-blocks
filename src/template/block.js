import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

import attributes from './attributes'
import BLOCKNAMEComponent from './components/BLOCKNAME'

registerBlockType('power-blocks/BLOCKNAME', {
	title: 'BLOCKTITLE',
	icon: 'star-filled',
	category: 'power-blocks',
	keywords: ['power'],
	attributes,
	edit(props) {
		return <BLOCKNAMEComponent {...props} />
	},
	save(props) {
		return <BLOCKNAMEComponent.Content {...props} />
	}
})
