const getInlineStyles = (attributes) => {
	return {
		backgroundImage: `url(${attributes.imageUrl})`,
		backgroundPosition: `${attributes.imageAlign} center`
	}
}
export { getInlineStyles }
