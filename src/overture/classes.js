import classnames from 'classnames'
import bem from '../util/bem'
import decorationsClasses from '../util/Decorations/classes'

const block = 'overture'

const getWrapperClasses = attributes => classnames(
	attributes.className,
	bem(block, 'wrap'),
	...decorationsClasses(attributes)
)

const getContainerClasses = attributes => block
const getElementClasses = (el, mods = []) => bem(block, el, mods)

export {
	getWrapperClasses,
	getContainerClasses,
	getElementClasses
}
