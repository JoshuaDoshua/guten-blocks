import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

import attributes from './attributes'
import Overture from './components/Overture'

registerBlockType('power-blocks/overture', {
	title: 'Overture',
	description: 'Hero image with overlapping text',
	category: 'power-blocks',
	icon: 'editor-table',
	attributes,
	edit(props) {
		return <Overture {...props} />
	},
	save(props) {
		return <Overture.Content {...props} />
	}
})
