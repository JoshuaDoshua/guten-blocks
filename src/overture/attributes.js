import { imageAttributes } from '../util/ImageSelector'

export default {
	imageAlign: {
		type: 'string',
		default: 'center'
	},
	...imageAttributes
}
