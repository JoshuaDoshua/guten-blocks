const { Component } = wp.element
const { InspectorControls } = wp.editor
const {
	PanelBody,
	PanelRow,
	SelectControl
} = wp.components

import { ImageSelector } from '../../util/ImageSelector'
import DecorationsOptions from '../../util/Decorations'

export default class OvertureInspector extends Component {
	constructor() {
		super(...arguments)
	}

	render() {
		const { attributes, setAttributes } = this.props

		return (
			<InspectorControls>
				<PanelBody
					title="Overture Image"
					initialOpen={true}
					>
					<PanelRow>
						<ImageSelector {...this.props} />
					</PanelRow>
					<PanelRow>
						<SelectControl
							label="Position"
							value={attributes.imageAlign}
							options={[
								{label: 'Top', value: 'top'},
								{label: 'Center', value: 'center'},
								{label: 'Bottom', value: 'bottom'}
							]}
							onChange={value => setAttributes({imageAlign: value})}
							/>
					</PanelRow>
				</PanelBody>
				<DecorationsOptions
					{...this.props}
					hideTab
					/>
			</InspectorControls>
		)
	}
}
