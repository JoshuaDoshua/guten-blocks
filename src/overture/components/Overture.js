const { Component, Fragment } = wp.element
const { InspectorControls, InnerBlocks } = wp.editor
const { PanelBody, PanelRow } = wp.components

import { ImageSelector } from '../../util/ImageSelector'
import OvertureInspector from './OvertureInspector'

import {
	getWrapperClasses,
	getContainerClasses,
	getElementClasses
} from '../classes'
import { getInlineStyles } from '../inline-styles'

export default class Overture extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const { 
			attributes,
			className,
			setAttributes
		} = this.props

		return (
			<Fragment>
				<OvertureInspector {...this.props} />

				<div className={getWrapperClasses(attributes)}>
					<div className={getElementClasses('img')}
						style={getInlineStyles(attributes)}
						></div>
					<div className={getContainerClasses()}>
						<div className={getElementClasses('inner')}>
							<InnerBlocks
								allowedBlocks={[
									'power-blocks/heading',
									'core/paragraph',
									'power-blocks/button',
									'power-blocks/accordion'
								]}
								template={[
									['power-blocks/heading', {}],
									['core/paragraph', {align: 'center'}]
								]}
								/>
						</div>
					</div>
				</div>
			</Fragment>
		)
	}

	static Content(props) {
		const { 
			attributes,
			className,
			setAttributes
		} = props

		return (
			<div className={getWrapperClasses(attributes)}>
				<div className={getElementClasses('img')}
					style={getInlineStyles(attributes)}
					></div>
				<div className={getContainerClasses()}>
					<div className={getElementClasses('inner')}>
						<InnerBlocks.Content />
					</div>
				</div>
			</div>
		)
	}
}
