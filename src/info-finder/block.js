import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks
const { ServerSideRender } = wp.components

registerBlockType('power-blocks/info-finder', {
	title: 'Info Finder',
	icon: 'schedule',
	category: 'power-blocks',
	keywords: ['power', 'blog', 'finder'],
	attributes: {},
	edit(props) {
		return (
			<ServerSideRender
				block="power-blocks/info-finder"
				attributes={{}}
				/>
		)
	},
	save() {
		return null
	}
});
