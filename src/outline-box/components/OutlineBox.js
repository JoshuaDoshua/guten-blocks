const { Component, Fragment } = wp.element
const { InnerBlocks, InspectorControls } = wp.editor

import DecorationsOptions from '../../util/Decorations'
import { getWrapperClasses } from '../classes'

export default class OutlineBox extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const { attributes } = this.props
		const classNames = getWrapperClasses(attributes)
		return (
			<Fragment>
				<InspectorControls>
					<DecorationsOptions {...this.props} />
				</InspectorControls>
				<div className={classNames}>
					<div className="box">
						<InnerBlocks
							allowedBlocks={[
								'power-blocks/heading',
								'core/paragraph',
								'power-blocks/accordion',
								'power-blocks/button',
								'power-blocks/columns'
							]}
							template={[['core/paragraph', {}]]}
							/>
					</div>
				</div>
			</Fragment>
		)
	}

	static Content(props) {
		const { attributes } = props
		const classNames = getWrapperClasses(attributes)

		return (
			<div className={classNames}>
				<div className="box">
				<InnerBlocks.Content />
				</div>
			</div>
		)
	}
}
