import { decorationsAttributes } from '../util/Decorations'

export default {
	...decorationsAttributes
}
