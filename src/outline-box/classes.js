import classnames from 'classnames'
import bem from '../util/bem'
import decorationsClasses from '../util/Decorations/classes'

const getWrapperClasses = attributes => classnames(
	attributes.className,
	'box__wrap',
	...decorationsClasses(attributes)
)

export { getWrapperClasses }
