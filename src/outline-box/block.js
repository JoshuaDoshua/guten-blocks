import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

import attributes from './attributes'
import OutlineBox from './components/OutlineBox'

registerBlockType('power-blocks/outline-box', {
	title: 'Outline Box',
	icon: 'feedback',
	category: 'power-blocks',
	keywords: ['power', 'box', 'outline'],
	attributes,
	edit(props) {
		return <OutlineBox {...props} />
	},
	save(props) {
		return <OutlineBox.Content {...props} />
	}
})
