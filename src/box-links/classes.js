import classnames from 'classnames'
import bem from '../util/bem'

const block = "boxlinks"

const getWrapperClasses = (additionalClasses) => classnames(
	additionalClasses,
	bem(block, 'wrap')
)

const getContainerClasses = (mods = []) => classnames(
	block,
	bem(block, null, mods)
)

const getElementClasses = (el, additionalClasses) => classnames(
	additionalClasses,
	bem(block, el, []) // no mods
)

export {
	getWrapperClasses,
	getContainerClasses,
	getElementClasses
}
