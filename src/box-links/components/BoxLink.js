const {	Component, Fragment } = wp.element
const { IconButton, DropdownMenu } = wp.components

import PageAdderAutocomplete from './PageAdderAutocomplete'
import BackgroundIconSelect from '../../util/BackgroundIconSelect'

import { getElementClasses } from '../classes'

// TODO this weird? needs investigation
const boxLinkKeys = ['title']
export { boxLinkKeys }

export default class BoxLink extends Component {
	constructor(props) {
		super(props)

		this.onInsertLink = this.onInsertLink.bind(this)
		this.onRemoveLink = this.onRemoveLink.bind(this)
		this.onMoveUp = this.onMoveUp.bind(this)
		this.onMoveDown = this.onMoveDown.bind(this)

		this.onChangeLink = this.onChangeLink.bind(this)
	}

	onInsertLink() {
		this.props.onInsertLink(this.props.index)
	}
	onRemoveLink() {
		this.props.onRemoveLink(this.props.index)
	}

	onMoveUp() {
		if (this.props.isFirst) return

		this.props.onMoveUp(this.props.index)
	}
	onMoveDown() {
		if (this.props.isLast) return

		this.props.onMoveDown(this.props.index)
	}

	getButtons() {
		const { attributes, totalLinks } = this.props

		return (
			<Fragment>
				<div className="curtain-edit-buttons">
					<BackgroundIconSelect
						onSelect={value => console.log(value)}
						/>
				</div>
				<div className="curtain-edit-buttons">
					<IconButton
						className="editor-block-mover__control"
						onClick={this.onMoveUp}
						icon="arrow-left-alt2"
						label="Move Link Left"
						aria-disabled={this.props.isFirst}
						/>
					<IconButton
						className="editor-block-mover__control"
						onClick={this.onMoveDown}
						icon="arrow-right-alt2"
						label="Move Link Right"
						aria-disabled={this.props.isLast}
						/>
					<IconButton
						className="power-blocks-repeatable--button"
						icon="trash"
						label="Remove Link"
						onClick={this.onRemoveLink}
						/>
					{(totalLinks < 4) &&
						<IconButton
							className="power-blocks-repeatable--button"
							icon="insert"
							label="Insert Link"
							onClick={this.onInsertLink}
							/>
					}
				</div>
			</Fragment>
		)
	}

	onChangeLink(value) {
		this.props.onChange(value, this.props.index)
	}

	// editor
	render() {
		const { attributes, isSelected } = this.props

		// DISAPPEARS IF UNFOCUS AFTER OPENING AUTOCOMPLETER
		return (
			<div className={getElementClasses('item')} key={attributes.id}>
				{!attributes.title &&
					<PageAdderAutocomplete
						onChangeLink={this.onChangeLink}
						/>
				}
				{!!attributes.title && 
					<Fragment>
						<strong
							className={getElementClasses('title')}
							>{attributes.title}</strong>
						<a
							className={getElementClasses('btn', 'btn')}
							href={attributes.url}
							>Learn More</a>
					</Fragment>
				}
				{isSelected && (
					<div class="power-blocks-curtain-controls-container">
						{this.getButtons()}
					</div>
				)}
			</div>
		)
	}

	// front-end
	static Content(link) {
		return (
			<div className={getElementClasses('item')} key={link.id}>
				<strong className={getElementClasses('title')}>{link.title}</strong>
			</div>
		)
	}

}
