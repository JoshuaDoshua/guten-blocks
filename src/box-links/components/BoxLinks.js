const { Component, Fragment } = wp.element
const { IconButton } = wp.components
const { RichText } = wp.editor

import classnames from 'classnames'

import BoxLink, { boxLinkKeys } from './BoxLink'

import {
	getWrapperClasses,
	getContainerClasses
} from '../classes'

export default class BoxLinks extends Component {
	// no arg, ...props
	constructor() {
		super(...arguments)

		this.state = {focus: ""}
		this.editorRefs = {}

		this.insertLink = this.insertLink.bind(this)
		this.removeLink = this.removeLink.bind(this)
		this.moveLinkUp = this.moveLinkUp.bind(this)
		this.moveLinkDown = this.moveLinkDown.bind(this)

		this.swapLinks = this.swapLinks.bind(this)

		this.onAddLinkClick = this.onAddLinkClick.bind(this)
		this.onChangeLink = this.onChangeLink.bind(this)
	}

	static generateID(prefix) {
		return `${prefix}-${new Date().getTime()}`
	}

	// insertQuestion should be called w/o args
	onAddLinkClick() {
		this.insertLink()
	}

	getLinks() {
		return this.props.attributes.links
			? this.props.attributes.links.slice()
			: []
	}

	// replace the link with the given index
	onChangeLink(value, index) {
		const links = this.getLinks()
		if (index >= links.length) return

		links[index] = value
		this.props.setAttributes({links})
	}

	insertLink(index) {
		const links = this.getLinks()

		if (index === undefined) {
			index = links.length - 1
		}

		let newLink = {id: BoxLinks.generateID("boxlink")}
		boxLinkKeys.forEach(key => {
			Object.assign(newLink, {
				[key]: null
			})
		})
		links.splice(index + 1, 0, newLink)

		this.props.setAttributes({links})
	}

	removeLink(index) {
		const links = this.getLinks()

		links.splice(index, 1)
		this.props.setAttributes({links})
	}

	swapLinks(index1, index2) {
		const links = this.getLinks()

		const link = links[index1]
		links[index1] = links[index2]
		links[index2] = link

		this.props.setAttributes({links})
	}

	moveLinkUp(index) {
		this.swapLinks(index, index - 1)
	}
	moveLinkDown(index) { 
		this.swapLinks(index, index + 1)
	}

	getAddLinkButton() {
		const { attributes: { links  } } = this.props
		if (links.length === 4) return null

		return (
			<div className="power-blocks-curtain-buttons">
				<IconButton
					icon="insert"
					onClick={this.onAddLinkClick}
					className="editor-inserter__toggle"
					>&nbsp;Add Link</IconButton>
			</div>
		)
	}

	getLinksContent() {
		// TODO, switch from {...} to {...this.props] in edit?
		const { attributes, isSelected } = this.props

		return (
			attributes.links.map((link, index) => {
				return (
					<BoxLink
						index={index}
						key={link.id}
						attributes={link}
						onInsertLink={this.insertLink}
						onRemoveLink={this.removeLink}
						onChange={this.onChangeLink}
						isSelected={isSelected}
						onMoveUp={this.moveLinkUp}
						onMoveDown={this.moveLinkDown}
						isFirst={index === 0}
						isLast={index === (attributes.links.length - 1)}
						totalLinks={attributes.links.length}
						/>
				)
			})
		)
	}

	render() {
		const { className, attributes, isSelected } = this.props
		return (
			<div className={getWrapperClasses(className)}>
				<div className={getContainerClasses()}>
					{ this.getLinksContent() }
				</div>
				{ isSelected && this.getAddLinkButton() }
			</div>
		)
	}

	static Content(attributes) {
		const { links, className } = attributes


		const linksContent = links ? links.map((link, index) =>
			<BoxLink.Content key={index} {...link} />
		) : "null"

		return (
			<div className={getWrapperClasses(className)}>
				<div className={getContainerClasses()}>
					{linksContent}
				</div>
			</div>
		)
	}


}
