const { Component } = wp.element
const { InspectorControls } = wp.editor
const { 
	PanelBody,
	PanelRow,
	SelectControl,
	Button
} = wp.components

import apiFetch from '@wordpress/api-fetch'
import DecorationOptions from '../../util/Decorations'

export default class BoxLinksInspector extends Component {
	constructor() {
		super(...arguments)

		this.state = {pages: []}
		this.fetchPages()
	}

	fetchPages() {
		console.log('fetching')
		apiFetch({path: '/wp-json/wp/v2/pages'}).then(pages => {
			let found = []
			pages.forEach(pg => {
				found.push({
					value: `${pg.link}:${pg.title.rendered}`,
					label: pg.title.rendered
				})
			})
			this.setState({pages: found})
		})
	}

	render() {
		const {
			attributes,
			setAttributes
		} = this.props

		return (
			<InspectorControls>
				<PanelBody
					title="Box Links"
					initialOpen={true}
					>
					<SelectControl
						label="Add Page Link"
						value={null}
						options={this.state.pages}
						onChange={val => console.log(val)}
						/>
					<Button isDefault>Add Page</Button>
				</PanelBody>
				<DecorationOptions {...this.props} />
			</InspectorControls>
		)
	}
}
