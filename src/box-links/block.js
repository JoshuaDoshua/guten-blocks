import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

import attributes from './attributes'
import BoxLinks from './components/BoxLinks'

registerBlockType('power-blocks/box-links', {
	title: 'Box Links',
	icon: 'star-filled',
	category: 'power-blocks',
	keywords: ['power', 'links', 'box'],
	attributes,
	edit({attributes, className, setAttributes, isSelected}) {
		return (
			<BoxLinks {...{attributes, className, setAttributes, isSelected}} />
		)
	},
	save({attributes}) {
		return <BoxLinks.Content {...attributes} />
	}
})
