import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

import attributes from './attributes'
import UpNextComponent from './components/UpNextComponent'

registerBlockType('power-blocks/up-next', {
	title: 'Up Next',
	icon: 'migrate',
	category: 'power-blocks',
	keywords: ['power', 'next', 'link'],
	attributes,
	edit(props) {
		return <UpNextComponent {...props} />
	},
	save(props) {
		return <UpNextComponent.Content {...props} />
	}
})
