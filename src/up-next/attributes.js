export default {
	title: {
		type: 'string'
	},
	ancestors: {
		type: 'array'
	},
	url: {
		type: 'string'
	}
}
