const { Component, Fragment } = wp.element
const { RichText } = wp.editor

import classnames from 'classnames'

import PageAdderAutocomplete from './PageAdderAutocomplete'
import UpNextTitles from './UpNextTitles'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'

export default class UpNextComponent extends Component {
	constructor(props) {
		super(props)

		this.onChangeLink = this.onChangeLink.bind(this)
	}

	onChangeLink(value) {
		this.props.setAttributes({
			title: value.title,
			url: value.url,
			ancestors: value.ancestors
		})
	}


	render() {
		const { attributes } = this.props
		// TODO classnames
		return (
			<div className="upnext__wrap">
				<a className="upnext" href="#">
					<strong className="subhead upnext__subhead">Up Next</strong>
					{!attributes.title &&
						<PageAdderAutocomplete
							onChangeLink={this.onChangeLink}
							/>
					}
					{!!attributes.title && <UpNextTitles {...this.props} />}
					<span className="upnext__icon">
						<FontAwesomeIcon icon={['fal', 'arrow-right']} />
					</span>
				</a>
			</div>
		)
	}

	static Content(props) {
		const { attributes, className } = props
		return (
			<div className="upnext__wrap">
				<a className="upnext" href={attributes.url}>
					<strong className="subhead upnext__subhead">Up Next</strong>
					<UpNextTitles {...props} />
					<span className="upnext__icon">
						<i className="fal fa-arrow-right"></i>
					</span>
				</a>
			</div>
		)
	}
}
