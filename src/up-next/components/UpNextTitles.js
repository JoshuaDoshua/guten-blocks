const { Component } = wp.element

export default class UpNextTitles extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const { title, ancestors } = this.props.attributes

		if (!title || !ancestors) return null

		const last = (<span className="upnext__item upnext__item--last">{title}</span>)

		if (ancestors.length === 0) return last

		const first = (<span className="upnext__item upnext__item--first">{ancestors[0]}</span>)
		const middle = ancestors.length > 1
			? (<span className="upnext__item upnext__item--mid">{ancestors[ancestors.length - 1]}</span>)
			: null

		return (
			<div className="headline upnext__headline">
				{first}
				{middle}
				{last}
			</div>
		)
	}
}
