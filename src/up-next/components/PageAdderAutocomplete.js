// TODO combinewith box-links duplicate
const { Component, Fragment } = wp.element
const { RichText } = wp.editor

import pageAutoComplete from '../../util/pageAutocomplete'

export default class PageAdderAutocomplete extends Component {
	constructor(props) {
		super(props)

		this.state = {value: null}

		this.onChange = this.onChange.bind(this)
		this.onReplace = this.onReplace.bind(this)
		this.getPageCompletion = this.getPageCompletion.bind(this)
	}

	onChange(value) {
		this.setState({value})
	}

	onReplace(value) {
		// always returns array
		this.props.onChangeLink(value[0])
	}

	getPageCompletion(page) {
		return {
			action: 'replace',
			value: {
				title: page.title.rendered,
				url: page.link,
				ancestors: page.ancestors
			}
		}
	}

	render() {
		pageAutoComplete.getOptionCompletion = this.getPageCompletion

		return (
			<RichText
				autocompleters={[pageAutoComplete]}
				placeholder="'#' to search"
				keepPlaceholderOnFocus
				value={this.state.value}
				onChange={this.onChange}
				onReplace={this.onReplace}
				multiline={true}
				onFocus={null}
				/>
		)
	}
}
