const {	Component, Fragment } = wp.element
const { IconButton } = wp.components
const { RichText } = wp.editor
const { MediaUpload } = wp.blocks

import isShallowEqual from "@wordpress/is-shallow-equal/objects"

const curtainSlideKeys = [
	'title', 'subtitle', 'heading', 'copy'
]
export { curtainSlideKeys }

export default class CurtainSlide extends Component {
	constructor(props) {
		super(props)

		this.onSelectImage = this.onSelectImage.bind(this)

		this.onMoveUp = this.onMoveUp.bind(this)
		this.onMoveDown = this.onMoveDown.bind(this)
	}

	onSelectImage(media) {
		const { index} = this.props
		// do iamge
	}

	onMoveUp() {
		if (this.props.isFirst) return

		this.props.onMoveUp(this.props.index)
	}
	onMoveDown() {
		if (this.props.isLast) return

		this.props.onMoveDown(this.props.index)
	}

	// these dont seem to be firing
	setTitleRef(ref) {
		console.log('setTitleRef')
		this.props.editorRef("title", ref, this.props.index)
	}
	setSubtitleRef(ref) {
		this.props.editorRef("subtitle", ref, this.props.index)
	}
	setHeadlineRef(ref) {
		this.props.editorRef("heading", ref, this.props.index)
	}
	setCopyRef(ref) {
		this.props.editorRef("copy", ref, this.props.index)
	}

	getButtons() {
		const { attributes, index } = this.props

		return (
			<div className="power-blocks-curtain-controls-container curtain__controls">
				<div className="curtain-edit-buttons">
					<IconButton
						className="power-blocks-repeatable--button"
						icon="format-image"
						label="Set Image"
						onClick={() => console.log('iage')}
						/>
				</div>
				<div className="curtain-edit-buttons">
					<IconButton
						className="editor-block-mover__control"
						onClick={this.onMoveUp}
						icon="arrow-up-alt2"
						label="Move Slide Up"
						aria-disabled={this.props.isFirst}
						/>
					<IconButton
						className="editor-block-mover__control"
						onClick={this.onMoveDown}
						icon="arrow-down-alt2"
						label="Move Slide Down"
						aria-disabled={this.props.isLast}
						/>
					<IconButton
						className="power-blocks-repeatable--button"
						icon="trash"
						label="Remove Slide"
						onClick={() => this.props.removeSlide(index)}
						/>
					<IconButton
						className="power-blocks-repeatable--button"
						icon="insert"
						label="Insert Slide"
						onClick={() => this.props.insertSlide(index)}
						/>
				</div>
			</div>
		)
	}

	// editor
	render() {
		const {
			attributes,
			index,
			subElement,
			isSelected
		} = this.props

		// TODO: make this only show the "active" slide using the arrows?
		return (
			<div className="curtain__item" key={attributes.id}
				hidden={index > 0 && !isSelected}
				>
				<div className="curtain__body">
					<RichText
						tagName="h3"
						className="curtain__headline"
						onChange={this.props.onChangeHeadline}
						value={this.props.headlineValue}
						placeholder="Headline"
						keepPlaceholderOnFocus={true}
						formattingControls={[]}
						/>
					<RichText
						className="curtain__heading"
						tagName="strong"
						unstableOnSetup={this.setHeadlineRef}
						key={attributes.id + "-heading"}
						value={attributes.heading}
						onChange={value => this.props.onChange('heading', value, index)}
						isSelected={isSelected && subElement === "heading"}
						setFocusedElement={() => this.props.onFocus('heading', index)}
						placeholder="Heading"
						keepPlaceholderOnFocus={true}
						formattingControls={[]}
						/>
					<RichText
						className="curtain__copy"
						tagName="p"
						unstableOnSetup={this.setCopyRef}
						key={attributes.id + "-copy"}
						value={attributes.copy}
						onChange={value => this.props.onChange('copy', value, index)}
						isSelected={isSelected && subElement === "copy"}
						setFocusedElement={() => this.props.onFocus('copy', index)}
						placeholder="Copy"
						keepPlaceholderOnFocus={true}
						formattingControls={[]}
						/>
				</div>
				<div className="curtain__slides">
					<div className="curtain__slide">
						<div className="curtain__title">
							<i>{index + 1}</i>
							<RichText
								tagName="span"
								className="curtain__title"
								data-slideindex={this.props.index + 1}
								unstableOnSetup={this.setTitleRef}
								key={attributes.id + "-title"}
								value={attributes.title}
								onChange={value => this.props.onChange('title', value, index)}
								isSelected={isSelected && subElement == "title"}
								setFocusedElement={() => this.props.onFocus('title', index)}
								placeholder="Title"
								keepPlaceholderOnFocus={true}
								formattingControls={[]}
								/>
						</div>
						<RichText
							className="curtain__subtitle"
							tagName="em"
							unstableOnSetup={this.setSubtitleRef}
							key={attributes.id + "-subtitle"}
							value={attributes.subtitle}
							onChange={value => this.props.onChange('subtitle', value, index)}
							isSelected={isSelected && subElement === "subtitle"}
							setFocusedElement={() => this.props.onFocus('subtitle', index)}
							placeholder="Subtitle"
							keepPlaceholderOnFocus={true}
							formattingControls={[]}
							/>
					</div>
				</div>
				{isSelected && this.getButtons()}
			</div>
		)
	}

	// front-end
	static Content(slide) {
		return (
			<div className="curtain__slide" key={slide.id}>
				<RichText.Content
					tagName="strong"
					className="curtain__title"
					key={slide.id + "-title"}
					value={slide.title}
					/>
				<RichText.Content
					tagName="em"
					className="curtain__subtitle"
					key={slide.id + "-subtitle"}
					value={slide.subtitle}
					/>
				<RichText.Content
					tagName="strong"
					className="curtain__heading"
					key={slide.id + "-heading"}
					value={slide.heading}
					/>
				<RichText.Content
					tagName="p"
					className="curtain__copy"
					key={slide.id + "-copy"}
					value={slide.copy}
					/>
			</div>
		)
	}

}
