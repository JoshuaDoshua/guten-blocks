const { Component } = wp.element
const { IconButton } = wp.components

import classnames from 'classnames'
import CurtainSlide from './CurtainSlide'

import appendSpace from '../../util/appendSpace'
const CurtainSlideContentWithAppendedSpace = appendSpace(CurtainSlide.Content)

import { curtainSlideKeys } from './CurtainSlide'

export default class Curtain extends Component {
	// no arg, ...props
	constructor(props) {
		super(props)

		this.state = {focus: ""}
		this.editorRefs = {}

		this.onChangeSlide = this.onChangeSlide.bind(this)
		this.onChangeHeadline = this.onChangeHeadline.bind(this)
		this.insertSlide = this.insertSlide.bind(this)
		this.removeSlide = this.removeSlide.bind(this)
		this.swapSlides = this.swapSlides.bind(this)
		this.setSlideRef = this.setSlideRef.bind(this)
		this.moveSlideUp = this.moveSlideUp.bind(this)
		this.moveSlideDown = this.moveSlideDown.bind(this)
		this.onAddSlideClick = this.onAddSlideClick.bind(this)
		this.setFocus = this.setFocus.bind(this)
	}

	static generateID(prefix) {
		return `${prefix}-${new Date().getTime()}`
	}

	// insertQuestion should be called w/o args
	onAddSlideClick() {
		this.insertSlide()
	}

	getSlides() {
		return this.props.attributes.slides
			? this.props.attributes.slides.slice()
			: []
	}

	// replace the slide with the given index
	onChangeSlide(key, value, index) {
		const slides = this.getSlides()
		if (value === "") value = null

		if (index >= slides.length) return

		slides[index] = Object.assign(slides[index], {[key]: value})

		this.props.setAttributes({slides})
	}

	onChangeHeadline(value) {
		this.props.setAttributes({headline: value})
	}

	insertSlide(index) {
		const slides = this.getSlides()

		if (index === undefined) {
			index = slides.length - 1
		}

		let lastIndex = slides.length - 1
		while (lastIndex > index) {
			curtainSlideKeys.forEach(key => {
				this.editorRefs[`${lastIndex + 1}:${key}`] = this.editorRefs[`${lastIndex}:${key}`]
			})
			lastIndex--
		}

		let newSlide = {id: Curtain.generateID("curtain-slide")}
		curtainSlideKeys.forEach(key => {
			Object.assign(newSlide, {
				[key]: null
			})
		})
		slides.splice(index + 1, 0, newSlide)

		this.props.setAttributes({slides})
	}

	removeSlide(index) {
		const slides = this.getSlides()

		slides.splice(index, 1)
		this.props.setAttributes({slides})

		delete this.editorRefs[`${index}:title`]
		delete this.editorRefs[`${index}:subtitle`]

		let nextIndex = index + 1;
		while (this.editorRefs[`${nextIndex}:title`] || this.editorRefs[`${nextIndex}:subtitle`]) {
			curtainSlideKeys.foreach(key => {
				this.editorRefs[`${lastIndex - 1}:${key}`] = this.editorRefs[`${nextIndex}:${key}`]
			})
			nextIndex++
		}

		const deletedIndex = slides.length
		curtainSlideKeys.forEach(key => {
			delete this.editorRefs[`${deletedIndex}:${key}`]
		})

		let fieldToFocus = `0:${curtainSlideKeys[0]}`
		if (this.editorRefs[`${index}:${curtainSlideKeys[0]}`]) {
			fieldToFocus = `${index}:${curtainSlideKeys[0]}`
		}
		this.setFocus(fieldToFocus)
	}

	setSlideRef(part, ref, index) {
		this.editorRefs[`${index}:${part}`] = ref
		console.log('do set slide ref')
		console.log(this.editorRefs)
	}

	setFocus(part, index) {
		const elementToFocus = `${index}:${part}`
		if (elementToFocus === this.state.focus) return

		this.setState({focus: elementToFocus})
		// TODO this fixes the placeholder issue
		// but breaks the controls container
		// this.state.focus = elementToFocus

		if (this.editorRefs[elementToFocus]) {
			this.editorRefs[elementToFocus].focus()
		}
	}
	
	swapSlides(index1, index2) {
		const slides = this.getSlides()

		const slide = slides[index1]
		slides[index1] = slides[index2]
		slides[index2] = slide

		curtainSlideKeys.forEach(key => {
			const keyEditorRef = this.editorRefs[`${index1}:${key}`]
			this.editorRefs[`${index1}:${key}`] = this.editorRefs[`${index2}:${key}`]
			this.editorRefs[`${index2}:${key}`] = keyEditorRef
		})

		this.props.setAttributes({slides})

		const [focusIndex, subElement] = this.state.focus.split(":")
		if (focusIndex === `${index1}`) {
			this.setFocus(subElement, index2)
		} else if (focusIndex === `${index2}`) {
			this.setFocus(subElement, index1)
		}
	}

	moveSlideUp(index) {
		this.swapSlides(index, index - 1)
	}
	moveSlideDown(index) { 
		this.swapSlides(index, index + 1)
	}

	getAddSlideButton() {
		return (
			<IconButton
				icon="insert"
				onClick={this.onAddSlideClick}
				className="editor-inserter__toggle"
				>&nbsp;Add Slide</IconButton>
		)
	}

	getSlidesContent() {
		const { attributes, isSelected } = this.props

		if (!attributes.slides) return null

		const [focusIndex, subElement] = this.state.focus.split(":")

		return (
			attributes.slides.map((slide, index) => {
				return (
					<CurtainSlide
						ndex={index}
						key={slide.id}
						attributes={slide}
						insertSlide={this.insertSlide}
						removeSlide={this.removeSlide}
						editorRef={this.setSlideRef}
						onChange={this.onChangeSlide}
						headlineValue={attributes.headline}
						onChangeHeadline={this.onChangeHeadline}
						onFocus={this.setFocus}
						subElement={subElement}
						isSelected={isSelected}
						onMoveUp={this.moveSlideUp}
						onMoveDown={this.moveSlideDown}
						isFirst={index === 0}
						isLast={index === (attributes.slides.length - 1)}
						/>
				)
			})
		)
	}

	render() {
		const { className, isSelected } = this.props
		const classNames = classnames("curtain", className)

		// TODO these are only adding when CurtainSlide.insertSlide
		//console.log(this.editorRefs)
		
		return (
			<div className={classNames}>
				{ this.getSlidesContent() }
				{isSelected && (
					<div className="power-blocks-curtain-buttons">{ this.getAddSlideButton() }</div>
				)}
			</div>
		)
	}

	static Content(attributes) {
		const { slides, className } = attributes

		const slidesContent = slides ? slides.map((slide, index) =>
			<CurtainSlide.Content key={index} {...slide} />
		) : "null"

		const classNames = classnames(className, "curtain")

		return (
			<div className={classNames}>
				{slidesContent}
			</div>
		)
	}


}
