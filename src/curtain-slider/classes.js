import classnames from 'classnames'
import bem from '../util/bem'

const block = 'curtain'

const getWrapperClasses = additionalClasses => classnames(
	additionalClasses,
	bem(block, 'wrap')
)

const getContainerClasses = attributes => block

const getElementClasses = (el, mods = []) => bem(block, el mods)
export {
	getWrapperClasses,
	getContainerClasses,
	getElementClasses
}
