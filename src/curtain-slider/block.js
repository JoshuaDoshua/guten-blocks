import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

import attributes from './attributes'
import Curtain from './components/Curtain'

registerBlockType('power-blocks/curtain-slider', {
	title: 'Curtain Slider',
	icon: 'star-empty',
	category: 'power-blocks',
	keywords: ['power', 'slider', 'curtain'],
	attributes,
	edit(props) {
		const { attributes } = props
		if (!attributes.slides || attributes.slides.length === 0) {
			attributes.slides = [{
				id: Curtain.generateID("curtain-slide"), 
				title: null,
				subtitle: null,
				headline: null,
				copy: null
			}]
		}

		return <Curtain {...props} />
	},
	save({attributes}) {
		return <Curtain.Content {...attributes} />
	}
})
