export default {
	headline: {
		source: 'html',
		selector: '.curtain__headline'
	},
	slides: {
		type: 'array',
		default: []
	}
}
