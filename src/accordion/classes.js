import classnames from 'classnames'
import bem from '../util/bem'

const block = 'accordion'

const getContainerClasses = (additionalClasses,isDeep) => classnames(
	additionalClasses,
	block,
	{[`${block}--active`]: isDeep}
)

const getElementClasses = (el, mods = []) => bem(block, el, mods)

export {
	getContainerClasses,
	getElementClasses
}
