export default {
	title: {
		type: 'html',
		source: 'html',
		selector: '.accordion__title'
	}
}
