const { withSelect } = wp.data
const { Component } = wp.element
const { RichText, InnerBlocks } = wp.editor

import AccordionIcon from './AccordionIcon'
import {
	getContainerClasses,
	getElementClasses
} from '../classes'

class Accordion extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const { attributes, className, isSelected } = this.props

		const isDeepSelected = (
			this.props.clientId === this.props.selectedBlock
			|| this.props.clientId === this.props.rootBlock
		)

		return (
			<div className={getContainerClasses(className, isDeepSelected)}>
				<div className={getElementClasses('toggle')}>
					<RichText
						className={getElementClasses('title')}
						tagName="strong"
						value={attributes.title}
						placeholder="Title"
						keepPlaceholderOnFocus
						formattingControls={[]}
						onChange={newTitle => this.props.setAttributes({title: newTitle})}
						/>
					<AccordionIcon />
				</div>
				<div className={getElementClasses('body')}>
					<InnerBlocks
						allowedBlocks={[
							'core/paragraph',
							'core/button'
						]}
						template={[['core/paragraph', {}]]}
						/>
				</div>
			</div>
		)
	}

	// TODO WITH SELECT MIGHT BREAK THIS
	static Content(props) {
		const {
			className,
			attributes: { title }
		} = props
		return (
			<div className={getContainerClasses(className)}>
				<div className={getElementClasses('toggle')}>
					<strong className={getElementClasses('title')}>{title}</strong>
					<AccordionIcon />
				</div>
				<div className={getElementClasses('body')}>
					<InnerBlocks.Content />
				</div>
			</div>
		)
	}
}

export default withSelect(select => {
	const { 
		getSelectedBlockClientId,
		getBlockRootClientId
	} = select('core/editor')

	const selectedBlock = getSelectedBlockClientId()
	const rootBlock = getBlockRootClientId(selectedBlock)

	return {selectedBlock,rootBlock}
})(Accordion)
