const { Component } = wp.element

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { getElementClasses } from '../classes'

export default class AccordionIcon extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		return (
			<div className={getElementClasses('icon')}>
				<span className="fa-stack fa-1x">
					<FontAwesomeIcon
						icon={['fal', 'square-full']}
						className="fa-stack-2x"
						/>
					<FontAwesomeIcon
						icon={['fal', 'plus']}
						className="fa-stack-1x"
						/>
					<FontAwesomeIcon
						icon={['fal', 'minus']}
						className="fa-stack-1x"
						/>
				</span>
			</div>
		)
	}
}
