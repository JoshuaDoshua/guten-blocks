import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

const { Fragment } = wp.element
const { InnerBlocks } = wp.editor

import attributes from './attributes'
import Accordion from './components/Accordion'
import AccordionIcon from './components/AccordionIcon'

import {
	getContainerClasses,
	getElementClasses
} from './classes'

registerBlockType('power-blocks/accordion', {
	title: 'Accordion',
	icon: 'feedback',
	category: 'power-blocks',
	keywords: ['power', 'accordion'],
	attributes,
	edit(props) {
		return (
			<Accordion {...props} />
		)
	},
	save({attributes, className}) {
		const { title } = attributes
		return (
			<div className={getContainerClasses(className)}>
				<div className={getElementClasses('toggle')}>
					<strong
						className={getElementClasses('title')}
						>{title}</strong>
					<AccordionIcon />
				</div>
				<div
					className={getElementClasses('body')}
					>
					<InnerBlocks.Content />
				</div>
			</div>
		)
	}
})
