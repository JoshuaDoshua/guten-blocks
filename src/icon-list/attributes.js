export default {
	items: {
		type: 'array',
		default: []
	},
	columns: {
		type: 'number',
		default: 1
	}
}
