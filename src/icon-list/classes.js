import classnames from 'classnames'
import bem from '../util/bem'

const block = 'iconlist'

const getWrapperClasses = additionalClasses => classnames(
	additionalClasses,
	bem(block, 'wrap')
)

const getContainerClasses = () => block

const getElementClasses = (el, mods = []) => bem(block, el, mods)

const getIconClasses = (icon, group = 'fal') => classnames(
	bem(block, 'item'),
	group,
	`fa-${icon}`
)

export {
	getWrapperClasses,
	getContainerClasses,
	getElementClasses,
	getIconClasses
}
