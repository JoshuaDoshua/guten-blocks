const { Component, Fragment } = wp.element
const { RichText } = wp.editor
const { IconButton } = wp.components

import {
	getElementClasses,
	getIconClasses
} from '../classes'

export default class IconListItem extends Component {
	constructor() {
		super(...arguments)

		this.handleMoveUp = this.handleMoveUp.bind(this)
		this.handleMoveDown = this.handleMoveDown.bind(this)
	}

	handleMoveUp() {
		if (this.props.isFirst) return
		this.props.onMoveUp(this.props.index)
	}
	handleMoveDown() {
		if (this.props.isLast) return
		console.log('do move down')
		this.props.onMoveDown(this.props.index)
	}
	handleRemove() {
		this.props.onRemoveItem(this.props.index)
	}
	handleInsert() {
		this.props.onInsertItem(this.props.index)
	}

	getButtons() {
		const { attributes, index, isFirst, isLast } = this.props

		return (
			<div className="power-blocks-curtain-controls-container curtain__controls">
				<div className="curtain-edit-buttons">
					[X]
				</div>
				<div className="curtain-edit-buttons">
					<IconButton
						className="editor-block-mover__control"
						icon="arrow-up-alt2"
						label="Move item up"
						onClick={this.handleMoveUp}
						aria-disabled={isFirst}
						/>
					<IconButton
						className="editor-block-mover__control"
						icon="arrow-down-alt2"
						label="Move item down"
						onClick={this.handleMoveDown}
						aria-disabled={isLast}
						/>
					<IconButton
						className="editor-block-mover__control"
						icon="trash"
						label="Remove item"
						onClick={() => this.props.onRemoveItem()}
						aria-disabled={isFirst && isLast}
						/>
					<IconButton
						className="editor-block-mover__control"
						icon="insert"
						label="Insert Item"
						onClick={() => this.props.onInsertItem(index)}
						/>
				</div>
			</div>
		)
	}

	render() {
		const { index, isSelected } = this.props
		const { copy, id } = this.props.attributes

		return (
			<div className={getElementClasses('item')}>
				<i className={getIconClasses('check')}></i>
				<RichText
					className={getElementClasses('copy')}
					tagName="span"
					key={id + "-copy"}
					value={copy}
					onChange={value => this.props.onChange(index, 'copy', value)}
					placeholder="Item copy..."
					keepPlaceholderOnFocus
					formattingControls={[]}
					/>
				{isSelected && this.getButtons()}
			</div>
		)
	}

	static Content(props) {
		const { attributes } = props

		return (
			<div className={getElementClasses('item')}>
				<i className={getIconClasses('check')}></i>
				<span className={getElementClasses('copy')}>{attributes.copy}</span>
			</div>
		)
	}
}
