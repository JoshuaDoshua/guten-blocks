const { Component, Fragment } = wp.element
const { RichText } = wp.editor
const { IconButton } = wp.components

import {
	getWrapperClasses,
	getContainerClasses,
	getElementClasses
} from '../classes'

import IconListItem from './IconListItem'

const iconListItemKeys = ['id', 'icon', 'copy']

export default class IconListComponent extends Component {
	constructor() {
		super(...arguments)

		this.getItems()

		this.changeItem = this.changeItem.bind(this)
		this.insertItem = this.insertItem.bind(this)
		this.removeItem = this.removeItem.bind(this)
		this.swapItems = this.swapItems.bind(this)
		this.moveItemUp = this.moveItemUp.bind(this)
		this.moveItemDown = this.moveItemDown.bind(this)

		this.onAddItemClick = this.onAddItemClick.bind(this)
	}

	getItems() {
		let { items } = this.props.attributes

		if (items.length === 0) {
			items = [{
				id: `item-${new Date().getTime()}`,
				icon: null,
				copy: null
			}]
			this.props.setAttributes({items})
		}

		return items.slice()
	}

	changeItem(index, key, value) {
		console.log(index,key,value)
		let items = this.getItems()

		if (index >= items.length) return
		if (value === "") value = null

		items[index] = Object.assign(items[index], {[key]: value})

		this.props.setAttributes({items})
	}

	insertItem(index) {
		let items = this.getItems()

		if (index === undefined) {
			index = items.length - 1
		}

		// do editor refs
		//let lastIndex = items.length - 1
		//while (lastIndex > index) {}

		let newItem = {id: `item-${new Date().getTime()}`}
		iconListItemKeys.forEach(key => Object.assign(newItem, {[key]: null}))
		items.splice(index + 1, 0, newItem)

		this.props.setAttributes({items})
	}

	removeItem(index) {
		let items = this.getItems()

		items.splice(index, 1)

		this.props.setAttributes({items})

		// do editor refs and reset focus
	}

	swapItems(index1, index2) {
		const items = this.getItems()
		const item = items[index1]
		items[index1] = items[index2]
		items[index2] = item

		// do editor refs
		
		this.props.setAttributes({items})

		// do focus
	}

	moveItemUp(index) {
		this.swapItems(index, index - 1)
	}
	moveItemDown(index) {
		this.swapItems(index, index + 1)
	}

	// must be called without args
	onAddItemClick() {
		this.insertItem()
	}

	getItemsContent() {
		const { attributes, isSelected } = this.props

		if (!attributes.items) return null

		return (
			attributes.items.map((item, index) => {
				return (
					<IconListItem
						index={index}
						key={item.id}
						attributes={item}
						onInsertItem={this.insertItem}
						onRemoveItem={this.removeItem}
						onChange={this.changeItem}
						onMoveUp={this.moveItemUp}
						onMoveDown={this.moveItemDown}
						isFirst={index === 0}
						isLast={index === (attributes.items.length - 1)}
						isSelected={isSelected}
						/>
				)
			})
		)
	}

	render() {
		const {
			attributes,
			className,
			isSelected,
			setAttributes
		} = this.props

		return (
			<div className={getWrapperClasses(className)}>
				<div className={getContainerClasses()}>
					{ this.getItemsContent() }
				</div>
			</div>
		)
	}

	static Content(props) {
		const { attributes, className } = props

		return (
			<div className={getWrapperClasses(className)}>
				<div className={getContainerClasses()}>
					<RichText.Content
						tagName="ul"
						className={getElementClasses('list')}
						value={attributes.values}
						multiline="li"
						/>
				</div>
			</div>
		)
	}
}
