import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

import attributes from './attributes'
import IconListComponent from './components/IconListComponent'

registerBlockType('power-blocks/icon-list', {
	title: 'Icon List',
	icon: 'list-view',
	category: 'power-blocks',
	keywords: ['power', 'list', 'icon'],
	attributes,
	edit(props) {
		return <IconListComponent {...props} />
	},
	save(props) {
		return <IconListComponent.Content {...props} />
	}
})
