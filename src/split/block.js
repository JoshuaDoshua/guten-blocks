import './style.scss';
import './editor.scss';

const { registerBlockType } = wp.blocks

import attributes from './attributes'
import {
	getWrapperClasses,
	getContainerClasses,
	getColClasses
} from './classes'
import Split from './components/Split'

registerBlockType('power-blocks/split', {
	title: 'Split',
	icon: 'image-flip-horizontal',
	category: 'power-blocks',
	keywords: ['image', 'split'],
	attributes,
	edit(props) {
		return <Split {...props} />
	},
	save(props) {
		return <Split.Content {...props} />
	}
});
