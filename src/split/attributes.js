import { imageAttributes } from '../util/ImageSelector'
import { decorationsAttributes } from '../util/Decorations'

export default {
	type: {
		type: 'string',
		default: 'overlap'
	},
	reverse: {
		type: 'boolean',
		default: false
	},
	...imageAttributes,
	...decorationsAttributes
}
