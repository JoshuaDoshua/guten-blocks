const { Component } = wp.element
const { PanelBody, Button } = wp.components
const { InspectorControls, MediaUpload } = wp.editor

import DecorationsOptions from '../../util/Decorations'
import ImageSelector from '../../util/ImageSelector'

export default class SplitInspector extends Component {
	constructor() {
		super(...arguments)
	}

	render() {
		const {
			attributes,
			setAttributes
		} = this.props

		return (
			<InspectorControls>

				<PanelBody
					title="Split Image"
					className="power-blocks__panel power-blocks__panel--image"
					initialOpen={true}
					>
					<ImageSelector {...this.props} />
				</PanelBody>

				<DecorationsOptions {...this.props} />
			</InspectorControls>
		)
	}
}
