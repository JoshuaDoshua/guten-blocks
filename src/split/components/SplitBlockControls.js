const { Component, Fragment } = wp.element
const { Toolbar } = wp.components
const { BlockControls } = wp.editor
const { SVG, Path } = wp.components

export default class SplitBlockControls extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const {
			attributes,
			setAttributes
		} = this.props
		return (
			<BlockControls>
				<Toolbar
						controls={[{
						icon: 'align-pull-left',
						title: 'Image on Left',
						isActive: attributes.reverse,
						onClick: () => setAttributes({reverse: true})
					}, {
						icon: 'align-pull-right',
						title: 'Image on Right',
						isActive: !attributes.reverse,
						onClick: () => setAttributes({reverse: false})
					}]}
					/>
				<Toolbar
					controls={[{
						icon: <SVG xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><Path d="M11.8,2v20H3.7H1.8V2h1.8H11.8z M12.2,2v20h10V2H12.2z"/></SVG>,
						title: 'Touching',
						isActive: attributes.type === 'touch',
						onClick: () => setAttributes({type: 'touch'})
					}, {
						icon: <SVG xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><Path d="M11,5.9v12.3h-0.8H1.8V5.9h8.3H11z M13.8,2H13v20h0.8h8.3V2H13.8z"/></SVG>,
						title: 'Separate',
						isActive: attributes.type === 'separate',
						onClick: () => setAttributes({type: 'separate'})
					}, {
						icon: <SVG xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><Path d="M22.2,2v20h-12v-1.9h5.9V3.9h-5.9V2H22.2z M10.2,5.9H1.8v12.2h8.3h3.9V5.9H10.2z"/></SVG>,
						title: 'Overlap',
						isActive: attributes.type === 'overlap',
						onClick: () => setAttributes({type: 'overlap'})
					}, {
						icon: <SVG xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><Path d="M22.2,2v20H1.8v-2.6h14.3V6.8H1.8V2H22.2z M10.2,8.8H1.8v8.5h8.3H14V8.8H10.2z"/></SVG>,
						title: 'Full',
						isActive: attributes.type === 'full',
						onClick: () => setAttributes({type: 'full'})
					}]}
					/>
			</BlockControls>
		)
	}
}
