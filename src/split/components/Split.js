const { Fragment, Component } = wp.element
const {	RichText, InnerBlocks } = wp.editor

import {
	getWrapperClasses,
	getContainerClasses,
	getColClasses
} from '../classes'

import SplitInspector from './SplitInspector'
import SplitBlockControls from './SplitBlockControls'

export default class SplitEdit extends Component {
	constructor() {
		super(...arguments)
	}

	render() {
		const {
			attributes,
			setAttributes,
			className
		} = this.props

		return (
			<Fragment>
				<SplitInspector {...this.props} />
				<SplitBlockControls {...this.props} />

				<div className={getWrapperClasses(attributes)}>
					<div className={getContainerClasses(attributes)}>
						<div className={getColClasses('body')}>
							<InnerBlocks
								allowedBlocks={[
									'power-blocks/heading',
									'core/paragraph',
									'power-blocks/button',
									'core/image'
								]}
								template={[
									['power-blocks/heading', {}],
									['core/paragraph', {}]
								]}
								/>
						</div>
						<div
							className={getColClasses('img')}
							style={{backgroundImage:`url(${attributes.imageUrl})`}}
							></div>
					</div>
				</div>
			</Fragment>
		)
	}

	static Content(props) {
		const { attributes } = props

		return (
			<div className={getWrapperClasses(attributes)}>
				<div className={getContainerClasses(attributes)}>
					<div className={getColClasses('body')}>
						<InnerBlocks.Content />
					</div>
					<div className={getColClasses('img')}
						style={{backgroundImage:`url(${attributes.imageUrl})`}}
						></div>
				</div>
			</div>
		)
	}
}
