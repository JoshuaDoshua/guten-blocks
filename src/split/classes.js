import classnames from 'classnames'
import bem from '../util/bem'
import { decorationsClasses } from '../util/Decorations'

const block = 'split'

const getWrapperClasses = attributes => {
	const decorationClasses = decorationsClasses(attributes)

	return classnames(
		bem(block, 'wrap'),
		...decorationClasses
	)
}

const getContainerClasses = attributes => classnames(
	bem(block, null, [attributes.type]),
	{[`${block}--reverse`]: attributes.reverse}
)

const getColClasses = mod => bem(block, 'col', [mod])

export {
	getWrapperClasses,
	getContainerClasses,
	getColClasses
}
