const layoutOptions = [{
	label: '[ 1 / 2 ] x2',
	value: 'halves',
}, {
	label: '[ 1 / 3 ] x3',
	value: 'thirds'
}, {
	label: '[ 1 / 4 ] x4',
	value: 'quarters'
}, {
	label: '[ 1 / 3 ] [ 2 / 3 ]',
	value: 'third-first'
}, {
	label: '[ 2 / 3 ] [ 1 / 3 ]',
	value: 'third-last',
}, {
	label: '[ 3 / 4 ] [ 1 / 4 ]',
	value: 'quarter-last',
}, {
	label: '[ 1 / 4 ] [ 3 / 4 ]',
	value: 'quarter-first',
}, {
	label: '[ 1 / 2 ] [ 1 / 4 ] x2',
	value: 'half-quarters'
}, {
	label: '[ 1 / 4 ] x2 [ 1 / 2 ]',
	value: 'quarters-half'
}]

const alignmentOptions = [{
	label: 'Top',
	value: 'top',
}, {
	label: 'Middle',
	value: 'middle',
}, {
	label: 'Bottom',
	value: 'bottom',
}, {
	label: 'Stretch',
	value: 'stretch',
}, {
	label: 'Baseline',
	value: 'baseline'
}]

export {
	layoutOptions,
	alignmentOptions	
}
