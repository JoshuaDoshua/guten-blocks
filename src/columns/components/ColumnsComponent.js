const { Component, Fragment } = wp.element
const { InnerBlocks } = wp.editor

import { getWrapperClasses, getContainerClasses } from '../classes'
import ColumnsInspector from './ColumnsInspector'

export default class ColumnsComponent extends Component {
	constructor() {
		super(...arguments)

		this.getColumnsTemplate = this.getColumnsTemplate.bind(this)
	}

	getColumnsTemplate() {
		const { layout } = this.props.attributes

		let template = [
			['power-blocks/column', {}],
			['power-blocks/column', {}]
		]

		// add a third
		if (['thirds', 'quarters', 'half-quarters', 'quarters-half'].includes(layout)) {
			template.push(['power-blocks/column', {}])
		}
		// add a fourth
		if (layout === 'quarters') {
			template.push(['power-blocks/column', {}])
		}

		return template
	}

	render() {
		const { attributes, className, setAttributes } = this.props

		return (
			<Fragment>
				<ColumnsInspector {...this.props} />
				<div className={getWrapperClasses(className)}>
					<div className={getContainerClasses(attributes)}>
						<InnerBlocks
							template={this.getColumnsTemplate()}
							templateLock="all"
							allowedBlocks={['power-blocks/column']}
							/>
					</div>
				</div>
			</Fragment>
		)
	}

	static Content(props) {
		const { attributes, className, setAttributes } = props

		return (
			<div className={getWrapperClasses(className)}>
				<div className={getContainerClasses(attributes)}>
					<InnerBlocks.Content />
				</div>
			</div>
		)
	}
}
