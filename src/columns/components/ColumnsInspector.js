const { Component, Fragment } = wp.element
const { PanelBody, SelectControl } = wp.components
const { InspectorControls } = wp.editor

import { layoutOptions, alignmentOptions } from '../options'

export default class ColumnsInspector extends Component {
	constructor() {
		super(...arguments)
	}

	render() {
		const { attributes, setAttributes } = this.props
		return (
			<InspectorControls>
				<SelectControl
					label="Layout"
					value={attributes.layout}
					options={layoutOptions}
					onChange={value => setAttributes({layout: value})}
					/>
				<SelectControl
					label="Alignment"
					value={attributes.alignment}
					options={alignmentOptions}
					onChange={value => setAttributes({alignment: value})}
					/>
			</InspectorControls>
		)
	}
}
