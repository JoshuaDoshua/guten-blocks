import classnames from 'classnames'
import bem from '../util/bem'

const block = 'columns'

const getWrapperClasses = additionalClasses => classnames(
	additionalClasses,
	bem(block, 'wrap')
)

const getContainerClasses = attributes => bem(block, null, [
	attributes.layout,
	attributes.alignment
])

export {
	getWrapperClasses,
	getContainerClasses
}
