export default {
	layout: {
		type: 'string',
		default: 'halves'
	},
	alignment: {
		type: 'string',
		default: 'stretch'
	}
}

