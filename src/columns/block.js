import './style.scss'
import './editor.scss'

const { SVG, G, Path } = wp.components

const { registerBlockType } = wp.blocks

import attributes from './attributes'
import ColumnsComponent from './components/ColumnsComponent'

registerBlockType('power-blocks/columns', {
	title: 'Columns',
	icon: <SVG viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><Path fill="none" d="M0 0h24v24H0V0z" /><G><Path d="M21 4H3L2 5v14l1 1h18l1-1V5l-1-1zM8 18H4V6h4v12zm6 0h-4V6h4v12zm6 0h-4V6h4v12z" /></G></SVG>,
	category: 'layout',
	keywords: ['power', 'layout', 'column'],
	attributes,
	edit(props) {
		return <ColumnsComponent {...props} />
	},
	save(props) {
		return <ColumnsComponent.Content {...props} />
	}
})
