const { Component, Fragment } = wp.element
const { Toolbar } = wp.components
const { BlockControls, RichText } = wp.editor

import BackgroundIconSelect from '../../util/BackgroundIconSelect'

export default class NumberFact extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const {
			attributes,
			setAttributes,
			isSelected
		} = this.props

		return (
			<Fragment>
				<BlockControls>
					<Toolbar>
						<BackgroundIconSelect
							onSelect={value => console.log(value)}
							/>
					</Toolbar>
				</BlockControls>
				<div className="numfact__wrap">
					<div className="numfact">
						<RichText
							className="numfact__number"
							tagName="strong"
							value={attributes.number}
							onChange={value => setAttributes({number: value})}
							formattingControls={[]}
							placeholder="###"
							keepPlaceholderOnFocus
							/>
						<RichText
							className="numfact__body"
							tagName="p"
							value={attributes.body}
							onChange={value => setAttributes({body: value})}
							formattingControls={[]}
							placeholder="Fact"
							keepPlaceholderOnFocus
							/>
					</div>
				</div>
			</Fragment>
		)
	}

	static Content(props) {
		const { attributes } = props
		return (
			<div className="numfact__wrap">
				<div className="numfact">
					<strong className="numfact__number">{attributes.number}</strong>
					<p className="numfact__body">{attributes.body}</p>
				</div>
			</div>
		)
	}
}
