export default {
	number: {
		type: 'string',
		selector: '.numfact__number'
	},
	body: {
		type: 'html',
		select: '.numfact__body'
	},
	backgroundIcon: {
		type: 'string',
		default: null
	}
}
