import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

import attributes from './attributes'
import NumberFact from './components/NumberFact'

registerBlockType('power-blocks/number-fact', {
	title: 'Number Fact',
	icon: 'megaphone',
	category: 'power-blocks',
	keywords: ['power', 'number', 'fact'],
	attributes,
	edit(props) {
		return <NumberFact {...props} />
	},
	save(props) {
		return <NumberFact.Content {...props} />
	}
})
