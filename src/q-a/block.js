import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

import attributes from './attributes'
import QaComponent from './components/QaComponent'

registerBlockType('power-blocks/q-a-block', {
	title: 'QA',
	icon: 'editor-help',
	category: 'power-blocks',
	keywords: ['power', 'qa', 'question'],
	attributes,
	edit(props) {
		return <QaComponent {...props} />
	},
	save(props) {
		return <QaComponent.Content {...props} />
	}
})
