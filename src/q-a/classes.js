import classnames from 'classnames'
import bem from '../util/bem'

const block = 'BEMBLOCK'

const getWrapperClasses = additionalClasses => classnames(
	additionalClasses,
	bem(block, 'wrap')
)

const getContainerClasses = attributes => classnames(
	block
)

const getElementClasses = (el, mods = []) => classnames(
	bem(block, el, mods)
)

export {
	getWrapperClasses,
	getContainerClasses,
	getElementClasses
}
