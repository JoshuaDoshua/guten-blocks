export default {
	question: {
		type: 'html',
		selector: '.qa__question'
	},
	answer: {
		type: 'html',
		selector: '.qa__answer'
	}
}
