const { Component } = wp.element
const { RichText } = wp.editor

export default class QaComponent extends Component {
	constructor(props) {
		super(props)
	}

	render() {
		const {
			attributes,
			setAttributes
		} = this.props

		return (
			<div className="qa__wrap">
				<div className="qa">
					<RichText
						className="qa__question"
						tagName="strong"
						value={attributes.question}
						onChange={value => setAttributes({question: value})}
						placeholder="Question"
						keepPlaceholderOnFocus
						/>
					<RichText
						className="qa__answer"
						tagName="p"
						value={attributes.answer}
						onChange={value => setAttributes({answer: value})}
						placeholder="Answer"
						keepPlaceholderOnFocus
						/>
				</div>
			</div>
		)
	}

	static Content(props) {
		const { attributes } = props

		return (
			<div className="qa__wrap">
				<div className="qa">
					<strong className="qa__question">{attributes.question}</strong>
					<p className="qa__answer">{attributes.answer}</p>
				</div>
			</div>
		)
	}
}
