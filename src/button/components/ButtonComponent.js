const { Component, Fragment } = wp.element
const { BlockControls, AlignmentToolbar, RichText } = wp.editor
const { Dashicon, IconButton } = wp.components

import classnames from 'classnames'

import UrlForm from '../../util/UrlForm'

import { getWrapperClasses, getElementClasses } from '../classes'

export default class ButtonComponent extends Component {
	constructor() {
		super(...arguments)

		this.nodeRef = null
		this.bindRef = this.bindRef.bind(this)
	}

	bindRef(node) {
		if (!node) return
		this.nodeRef = node
	}

	render() {
		const {
			attributes,
			className,
			setAttributes,
			isSelected
		} = this.props

		return (
			<Fragment>
				<BlockControls>
					<AlignmentToolbar
						value={attributes.alignment}
						onChange={value => setAttributes({alignment: value})}
						/>
				</BlockControls>
				<div className={getWrapperClasses(this.props)}>
					<RichText
						className={getElementClasses(attributes)}
						placeholder="Button text..."
						value={attributes.text}
						onChange={value => setAttributes({text: value})}
						keepPlaceholderOnFocus
						/>
					{isSelected && (
						<UrlForm
							urlValue={attributes.url}
							onChangeUrl={value => setAttributes({url: value})}
						/>
					)}
				</div>
			</Fragment>
		)
	}

	static Content(props) {
		const { attributes, className } = props
		const classes = classnames(className, "btn", "align--" + attributes.alignment)

		return (
			<div className={getWrapperClasses(props)}>
				<RichText.Content
					tagName="a"
					className={getElementClasses(attributes)}
					href={attributes.url}
					title={attributes.title}
					value={attributes.text}
					/>
			</div>
		)
	}
}
