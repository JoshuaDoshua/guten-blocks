export default {
	url: {
		type: 'string',
		source: 'attribute',
		selector: 'a',
		attribute: 'href'
	},
	title: {
		type: 'string',
		source: 'attribute',
		selector: 'a',
		attribute: 'title'
	},
	text: {
		type: 'string',
		source: 'html',
		selector: 'a'
	},
	alignment: {
		type: 'string',
		default: 'left'
	}
}
