import React, { Component } from 'react'

const { G, Path, SVG } = wp.components

export default class ButtonIcon extends Component {
	render() {
		return (
			<SVG viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
				<Path fill="none" d="M0 0h24v24H0V0z" />
				<G><Path d="M19 6H5L3 8v8l2 2h14l2-2V8l-2-2zm0 10H5V8h14v8z" /></G>
			</SVG>
		)
	}
}
