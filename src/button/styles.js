export default [
	{name: 'default', label: 'Default', isDefault: true},
	{name: 'primary', label: 'Primary'},
	{name: 'secondary', label: 'Secondary'},
	{name: 'ghost', label: 'Ghost'}
]
