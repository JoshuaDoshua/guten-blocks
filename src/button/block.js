import './style.scss'
import './editor.scss'

const { registerBlockType } = wp.blocks

import icon from './icon'
import styles from './styles'
import attributes from './attributes'
import ButtonComponent from './components/ButtonComponent'

registerBlockType('power-blocks/button', {
	title: 'Button',
	category: 'power-blocks',
	icon,
	attributes,
	styles,
	edit(props) {
		console.log(props)
		return <ButtonComponent {...props} />
	},
	save(props) {
		//return <p>edit</p>
		return <ButtonComponent.Content {...props} />
	}
})
