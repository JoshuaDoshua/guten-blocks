import classnames from 'classnames'
import bem from '../util/bem'

const block = 'btn'

const getWrapperClasses = props => classnames(
	props.className,
	bem(block, 'wrap', [props.attributes.alignment]),
)

const getElementClasses = attributes => {
	const isStyle = RegExp(/is-style-/)
	const style = isStyle.test(attributes.className)
		? attributes.className.replace(isStyle, '')
		: null

	return bem(block, null, [style])
}

export {
	getWrapperClasses,
	getElementClasses
}
