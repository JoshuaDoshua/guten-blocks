import apiFetch from '@wordpress/api-fetch'

export default {
	name: 'pages',
	className: 'editor-autocompleters__page',
	triggerPrefix: '#',
	options(search) {
		let payload = ''
		if (search) {
			payload = '?search=' + encodeURIComponent(search)
		}
		return apiFetch({path: '/wp-json/wp/v2/pages' + payload})
	},
	isDebounced: true,
	getOptionKeywords: page => [page.title.rendered],
	getOptionLabel: page => page.title.rendered,
	// getOptionCompletion: define in usage
}
