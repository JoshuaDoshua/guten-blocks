const { Component } = wp.element
const { DropdownMenu } = wp.components

export default class BackgroundIconSelect extends Component {
	constructor(props) {
		super(props)

		this.handleOnClick = this.handleOnClick.bind(this)
	}

	handleOnClick(value) {
		this.props.onSelect(value)
	}

	render() {
		return (
			<DropdownMenu
				icon="format-image"
				label="Background Icon"
				controls={[{
					title: 'Microscope',
					icon: 'search',
					onClick: () => this.handleOnClick('search')
				}, {
					title: 'Life preserver',
					icon: 'sos',
					onClick: () => this.handleOnClick('sos')
				}, {
					title: 'Tree',
					icon: 'palmtree',
					onClick: () => this.handleOnClick('tree')
				}]}
				/>
		)
	}
}
