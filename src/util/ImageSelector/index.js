const { Component } = wp.element
const { MediaUpload } = wp.editor
const { Button } = wp.components

import imageAttributes from './attributes'
export { imageAttributes }

export default class ImageSelector extends Component {
	constructor(props) {
		super(props)
	}

	getImageButton(openEvent) {
		const { attributes } = this.props

		if (attributes.imageUrl) {
			return (
				<img
					src={attributes.imageUrl}
					onClick={openEvent}
					className="inspector-bynum-preview"
					/>
			)
		}

		return (
			<div className="button-container">
				<Button
					onClick={openEvent}
					className="button button-large"
					>Pick an Image</Button>
			</div>
		)
	}

	render() {
		const {
			attributes,
			setAttributes
		} = this.props

		return (
			<MediaUpload
				onSelect={media => setAttributes({imageUrl: media.url})}
				type="image"
				value={attributes.imageId}
				render={({open}) => this.getImageButton(open)}
				/>
		)
	}
}

export {
	ImageSelector,
	ImageAttributes
}
