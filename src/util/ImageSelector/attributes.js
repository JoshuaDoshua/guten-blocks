export default {
	imageId: {
		type: 'number'
	},
	imageUrl: {
		type: 'string'
	},
	imageAlt: {
		type: 'string'
	}
}
