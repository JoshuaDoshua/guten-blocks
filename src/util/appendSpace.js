import React, {Fragment} from 'react'

const appendSpace = function(WrappedComponent) {
	return class ComponentWithAppendedSpace extends React.Component {
		render() {
			return (
				<Fragment>
					<WrappedComponent {...this.props} />
					{" "}
				</Fragment>
			)
		}
	}
}

export default appendSpace
