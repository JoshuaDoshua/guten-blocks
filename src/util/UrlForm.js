const { Component } = wp.element
const { RichText, URLInput } = wp.editor
const  { Dashicon, IconButton } = wp.components

export default class UrlForm extends Component {
	constructor() {
		super(...arguments)
	}

	render() {
		const {
			urlValue,
			onChangeUrl
		} = this.props

		return (
			<form
				className="block-library-button__inline-link"
				onSubmit={e => e.preventDefault()}
				>
				<Dashicon icon="admin-links" />
				<URLInput
					value={urlValue}
					onChange={onChangeUrl}
					/>
				<IconButton
					icon="editor-break"
					label="Apply"
					type="submit"
					/>
			</form>
		)
	}
}
