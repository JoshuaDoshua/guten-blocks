import classnames from 'classnames'

function decorationsClasses(attributes) {
	const tabClass = `decoration--tab--${attributes.tab}`
	const branchTypeClass = `decoration--branch--${attributes.branchType}`
	const branchLocClass = `decoration--branch--${attributes.branchLocation}`
	const bgImgClass = `decoration--background-image`

	return [
		{[tabClass]: attributes.tab !== 'none'},
		{[branchTypeClass]: attributes.branchType},
		{branchLocClass: attributes.branchLocation},
		{bgImgClass: attributes.backgroundImage}
	]
}

export default decorationsClasses
