
function decorationsInlineStyles(props) {
	return {
		backgroundImage: props.attributes.hasBackgroundImage
			? `url(${props.attributes.backgroundImage.url})`
			: null
	}
}

export default decorationsInlineStyles
