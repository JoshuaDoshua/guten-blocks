// need to include video?

const decorationsAttributes = {
	tab: {
		type: 'string',
		default: 'none'
	},
	branchType: {
		type: 'string',
		default: null
	},
	branchLocation: {
		type: 'string',
		default: null
	},
	backgroundImage: {
		type: 'string',
		default: null
	}
}

export default decorationsAttributes
