// external dependencies
const { MediaUpload } = wp.blocks
const {
	PanelBody,
	PanelRow,
	SelectControl,
	DropdownMenu,
	BaseControl,
	Button,
} = wp.components
const { Component, Fragment } = wp.element

import ImageSelector from '../ImageSelector'

// internal dependencies
import decorationsAttributes from './attributes'
import decorationsClasses from './classes'
import decorationsInlineStyles from './inline-styles'

export {
	decorationsAttributes,
	decorationsClasses,
	decorationsInlineStyles
}

export default class DecorationsOptions extends Component {
	constructor() {
		super(...arguments)
	}

	render() {
		const {
			attributes,
			setAttributes,
			hideTab
		} = this.props

		return (
			<Fragment>
				{!hideTab && (
				<PanelBody
					title="Decorations: Tab"
					className="power-blocks__panel power-blocks__panel--decorations"
					initialOpen={false}
					>
					<SelectControl
						label="Tab"
						value={attributes.tab}
						options={[
							{label: 'None', value: 'none'},
							{label: 'Left', value: 'left'},
							{label: 'Right', value: 'right'}
						]}
						onChange={newTab => setAttributes({tab: newTab})}
						/>
				</PanelBody>
				)}
				<PanelBody
					title="Decorations: Branch"
					className="power-blocks__panel power-blocks__panel--decorations"
					initialOpen={false}
					>
					<BaseControl
						id="decrations-branch-select"
						label="Branch Type"
						>
						<ul className="decorations-branch__list"
							style={{display: 'flex', alignItems: 'center', justifyContent: 'space-between'}}
							>
							<li id="decoration-branch-simple"
								onClick={() => setAttributes({branchType: 'simple'})}
								>Simple</li>
							<li id="decoration-branch-skinny"
								onClick={() => setAttributes({branchType: 'skinny'})}
								>Skinny</li>
							<li id="decoration-branch-arms"
								onClick={() => setAttributes({branchType: 'arms'})}
								>Arms</li>
							<li id="decoration-branch-forked"
								onClick={() => setAttributes({branchType: 'forked'})}
								>Forked</li>
						</ul>
					</BaseControl>
					<PanelRow>
						<DropdownMenu
							icon="editor-expand"
							label="Branch Location"
							controls={[{
								title: 'Top Left',
								icon: 'arrow-up-alt',
								onClick: () => console.log('top-left')
							}, {
								title: 'Top Right',
								icon: 'arrow-up-alt',
								onClick: () => console.log('top-right')
							}, {
								title: 'Bottom Right',
								icon: 'arrow-up-alt',
								onClick: () => console.log('bottom-right')
							}, {
								title: 'Bottom Left',
								icon: 'arrow-up-alt',
								onClick: () => console.log('bottom-left')
							}]}
							/>
						<Button
							isDefault
							onClick={() => setAttributes({branchType: null, branchLocation: null})}
							style={{display: attributes.branchType ? 'inline-block' : 'none'}}
							>Remove Branch</Button>
					</PanelRow>
				</PanelBody>
				<PanelBody 
					title="Decorations: BG Image"
					className="power-blocks__panel power-blocks__panel--decorations"
					initialOpen={false}
					>
				</PanelBody>
			</Fragment>
		)
	}
}

// function DecorationsOptions(props) {

// 	const setBranchType = newBranchType => setAttributes({branchType: newBranchType})
// 	const setBranchLocation = newBranchLocation => setAttributes({branchLocation: newBranchLocation})
// 	const setBackgroundImage = newBackgroundImage => setAttributes({backgroundImage: newBackgroundImage})

// }
