const { Component } = wp.element

import classnames from 'classnames'

export default class FontAwesomeIcon extends Component {
	constructor(props) {
		super(props)

		if (!this.props.group) {
			this.props.group = "fal"
		}
	}

	render() {
		const { group, name, additionalClasses } = this.props

		const classNames = classnames(group, `fa-${name}`, additionalClasses)

		return <i className={classNames} />
	}
}
