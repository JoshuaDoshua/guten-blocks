import classnames from 'classnames'

function bem(block = '', element = null, mods = []) {
	const classes = []
	const base = element
		? `${block}__${element}`
		: `${block}`

	classes.push(base)

	mods.forEach(mod => {
		classes.push(`${base}--${mod}`)
	})

	return classnames(classes)
}

export default bem
