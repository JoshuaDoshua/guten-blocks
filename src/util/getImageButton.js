const { Button } = wp.components

function getImageButton(openEvent, imageUrl) {

	console.log(openEvent, imageUrl)

	if (imageUrl) {
		return (
			<img
				src={imageUrl}
				onClick={openEvent}
				className="inspector-image-preview"
				/>
		)
	}

	return (
		<div className="button-container">
			<Button
				onClick={openEvent}
				className="button button-large"
				>Pick an Image</Button>
		</div>
	)
}

export default getImageButton
