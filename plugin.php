<?php
/**
 * Plugin Name: Power Blocks
 * Description: Custom Gutenberg Blocks
 * Version: 0.0.1
 * Author: Power Agency
 * Author Uri: <mailto:interactivetech@poweragency.com>
 *
 * @package power-blocks
 */

// Exit if accessed directly.
if (!defined('ABSPATH')) exit;

add_filter('block_categories', function($categories) {
	return array_merge($categories, [[
		'slug' => 'power-blocks',
		'title' => 'Power Blocks',
		'icon' => null
	]]);
}, 10, 2);

add_action('enqueue_block_editor_assets', function() {
	wp_enqueue_script(
		'power-blocks-editor-script',
		plugins_url('/dist/blocks.build.js', __FILE__),
		['wp-dom-ready', 'wp-blocks', 'wp-element', 'wp-editor'],
		true
	);
	wp_enqueue_style(
		'power-blocks-editor-styles',
		plugins_url('dist/blocks.editor.build.css', __FILE__),
		['wp-edit-blocks']
	);
});
add_action('enqueue_block_assets', function() {
	wp_enqueue_style(
		'power-blocks-styles',
		plugins_url('dist/blocks.style.build.css', __FILE__),
		['wp-editor']
	);
});

add_action('after_theme_setup', function() {
	add_theme_support('align-wide');
});

// add ancestors titles to response
add_action('rest_api_init', function() {
	register_rest_field('page', 'ancestors', [
		'get_callback' => function($page_arr) {
			$ancestors = get_post_ancestors($page_arr['id']);
			if (!$ancestors) return null;
			$ancestors = array_reverse($ancestors);
			$titles = [];
			foreach ($ancestors as $ancestor):
				$titles[] = get_the_title($ancestor);
			endforeach;

			return $titles;
		}
	]);
});

add_action('init', function() {
	register_block_type('power-blocks/children', [
		'attributes' => [
			'parent' => ['type' => 'number']
		],
		'render_callback' => 'power_render_block_children'
	]);
	register_block_type('power-blocks/info-finder', [
		'render_callback' => 'power_render_block_info_finder'
	]);
});

function power_render_block_children($attributes) {
	global $post;

	$children = new WP_Query([
		'post_type' => 'page',
		'posts_per_page' => -1,
		'post_parent' => $attributes['parent']
	]);

	if (!$children->have_posts()) return "<p>No children found</p>";
ob_start(); ?>

<div class="children">
	<?php while ($children->have_posts()): $children->the_post(); ?>
		<?php 
			$thumb = get_post_thumbnail_id($post->ID);
			$img = wp_prepare_attachment_for_js($thumb);
		?>
		<article class="children__item"
			style="background-image:url(<?= $img['sizes']['medium']['url']; ?>)"
			>
			<?php part('blocks.image.template'); ?>
			<a class="children__link"
				href="<?php the_permalink(); ?>"
				>
				<span class="children__icon">
					<i class="fal fa-arrow-right"></i>
				</span>
				<span class="children__text">
					<?php the_title(); ?>
				</span>
			</a>
		</article>
	<?php endwhile; // children have posts ?>
</div>

<?php
	wp_reset_query();
	return ob_get_clean();
}

function power_render_block_info_finder($attributes) {
	$audiences = get_terms('post_audience', ['hide_empty' => false]);
	$regions = get_terms('post_region', ['hide_empty' => false]);
	$tags = get_terms('post_tag', ['hide_empty' => false]);

	query_posts([
		'post_type' => 'post',
		'posts_per_page' => 6,
		'category__not_in' => [20]
	]);
	ob_start(); ?>
<div class="finder__wrap">
	<section class="finder">
		<form class="finder__body"
			flex="middle center"
			>
			<strong>I am a</strong>

			<div class="<?= bem('finder', 'dropdown', ['single','audience']); ?> dropdown dropdown--single">
				<span class="dropdown__title">
					<span>Audience</span>
					<i class="fal fa-angle-down fa-2x"></i>
				</span>
				<ul class="dropdown__list">
					<?php foreach ($audiences as $a): ?>
						<li class="<?= bem('dropdown', 'option', null); ?>"
							data-value="<?= $a->term_id; ?>"
							data-label="<?= $a->name; ?>"
							>
							<input class="<?= bem('dropdown', 'checkbox', 'hidden'); ?>"
								type="radio"
								name="audience"
								value="<?= $a->term_id; ?>"
								/>
							<span class="dropdown__label"><?= $a->name; ?></span>
						</li>
					<?php endforeach; // audiences ?>
				</ul>
			</div>

			<strong>from the</strong>

			<div class="<?= bem('finder', 'dropdown', 'region'); ?> dropdown dropdown--single">
				<span class="dropdown__title">
					<span>Region</span>
					<i class="fal fa-angle-down fa-2x"></i>
				</span>
				<ul class="dropdown__list">
					<?php foreach ($regions as $r): ?>
						<li class="<?= bem('dropdown', 'option', null); ?>"
							data-value="<?= $r->term_id; ?>"
							data-label="<?= $r->name; ?>"
							>
							<input class="<?= bem('dropdown', 'checkbox', 'hidden'); ?>"
								type="radio"
								name="audience"
								value="<?= $r->term_id; ?>"
								/>
							<span class="dropdown__label"><?= $r->name; ?></span>
						</li>
					<?php endforeach; // audiences ?>
				</ul>
			</div>

			<strong>looking for</strong>

			<div class="<?= bem('finder', 'dropdown', 'tag'); ?> dropdown dropdown--single">
				<span class="dropdown__title">
					<span>Tag</span>
					<i class="fal fa-angle-down fa-2x"></i>
				</span>
				<ul class="dropdown__list">
					<?php foreach ($tags as $t): ?>
						<li class="<?= bem('dropdown', 'option', null); ?>"
							data-value="<?= $t->term_id; ?>"
							data-label="<?= $t->name; ?>"
							>
							<input class="<?= bem('dropdown', 'checkbox', 'hidden'); ?>"
								type="radio"
								name="audience"
								value="<?= $t->term_id; ?>"
								/>
							<span class="dropdown__label"><?= $t->name; ?></span>
						</li>
					<?php endforeach; // audiences ?>
				</ul>
			</div>

			<strong>information.</strong>

			<button class="btn btn--lg btn--ghost finder__btn"
				type="submit"
				>
				<span class="btn__text">Find</span>
				<span class="btn__icon"><i class="fal fa-arrow-right"></i></span>
			</button>

		</form>
	</section>
</div>
<div flex>
	<?php
		while (have_posts()): the_post();
			part('resources.list.blog.item');
		endwhile; // have_posts
		wp_reset_query();
	?>
</div>

	<?php
	wp_reset_query();
	return ob_get_clean();
}
